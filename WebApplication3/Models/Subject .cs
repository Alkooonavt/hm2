﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication3.Models
{
    public class Subject
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("Название курса")]
        [Required]
        public  string CourseName { get; set; }
        [DisplayName("Аудитория")]
        public string Room { get; set; }
        [Display(Name = "Студенты")]
        public  IList<Student> Students { get; set; }=new List<Student>();
    }
}           