﻿namespace WebApplication3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateModelSubject : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Subjects", "CourseName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subjects", "CourseName", c => c.String());
        }
    }
}
